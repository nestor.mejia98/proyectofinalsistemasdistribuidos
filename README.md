# Proyecto Final Sistemas Distribuidos y Programación en Paralelo

**Integrantes**

- Nestor Ulises Mejía Cañas.
- Sergio Andrés Noh Puch.
- Fernando David Vásquez Rivas.

**Descripción**

Nuestro proyecto final de esta asignatura consiste en la recepción de datos del juego Formula 1 2018 de Codemasters mediante udp, su deserialización y procesamiento para ser usado posteriormente en un simulador 6DOF (6 grados de libertad) y la exposición de esos datos para consumo de cualquier cliente que lo desee.

**Partes**

- DataAccess:
Solución en .NET C# por la cual se reciben datos mediante udp, se deserializan, se publican a una cola en un servidor RabbitMQ y también se envía datos al puerto usb para el movimiento del simulador, consta de los siguientes proyectos:

    - ArduinoConnection: Proyecto que permite la conexión y el envío de datos al programa que se encuentra en la placa Arduino para la representación del movimiento del coche en el juego, en la estructura.

    - Entities: Contiene la clase TelemetryPacket, que es la estructura que se utiliza en otros proyectos de la solución.

    - ServiceBrokerPublisher: Serializa la clase TelemetryPacket a un JSON y pasa este JSON a un String para su publicación en RabbitMQ.

    - UdpReceiver: Abre un EndPoint que se mantiene a la escucha de paquetes udp, los recibe, deserealiza y dispara un evento para que los métodos suscritos procesen la data para su uso.

- NodeServer:
Servicio en JS con NodeJS encargado de ofrecer un endpoint de los datos en formato JSON al que otros clientes pueden acceder. Consiste en un script que se subscribe a una cola de mensajes de RabbitMQ para luego serializar los datos recibidos en un archivo con formato JSON para luego ser consumido como API. Es necesario correr el servicio desde la consola con el comando *node WebClients/dataReceiver.js*.

- JavaScriptDashboard:
*Se necesita el servicio en NodeJS corriendo*. Cliente indirecto de la fuente de datos y directo del servicio en NodeJS, consulta continuamente los datos para actualizar un dashboard representando la información en tiempo real. Este a su vez guarda los datos en un servicio de bases de datos en la nube (Firebase), paar que este funcione se deben introducir las credenciales en el archivo *WebClients/app/scripts/firebase.js*.

- ArduinoMovement:
*Se necesita el servicio en C# corriendo y una placa arduino con almenos 6 puertos pwm*. A la placa arduino se le debe cargar el código que esta en la ruta:
*proyectofinalsistemasdistribuidos\ArduinoMovement\ArduinoComputerCommunication*
Y en la Clase ArduinoConnection se debe definir que puerto se le asigno al arduino para que se pueda comunicar con el servicio.
El código del arduino esta pensado para una plataforma de stewart, la cual tiene 6 graqdos de libertad (osea 6 servomotores)

- MobileDashboard:
Aplicación programada en React Native (JavaScript) la cual muestra el tiempo por vuelta, marcha y velocidad del coche acorde a lo que se hace en el juego. Apunta a la url donde se encuentra el json actualizado con los datos más recientes.
