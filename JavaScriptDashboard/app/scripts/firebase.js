$(document).ready(function () {
    // Initialize Firebase
    var config = {
        apiKey: "",
        authDomain: "",
        databaseURL: "",
        projectId: "",
        storageBucket: "",
        messagingSenderId: ""
    };
    firebase.initializeApp(config);
});

var counter = 0;
setInterval(function () {
    $.getJSON("data/data.json", function (json) {
        firebase.database().ref('session/' + counter).set(json);
    });
    counter++;
}, 5000);