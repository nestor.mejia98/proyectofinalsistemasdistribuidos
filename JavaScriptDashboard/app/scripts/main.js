var refreshRate = 300;
var initJSON = $.getJSON("data/initData.json", function (json) {
    return json
});

$(document).ready(function () {

    // Initialize Firebase
    var config = {
        apiKey: "",
        authDomain: "",
        databaseURL: "",
        projectId: "",
        storageBucket: "",
        messagingSenderId: ""
    };
    firebase.initializeApp(config);

    drawGoogleChart(initJSON);
    updateLap(initJSON);
    updateGear(initJSON);
    drawForcesChart(initJSON);
    updateSteering(initJSON);
    updateWheelTemperature(initJSON);
});
//WHEELS
function updateWheelTemperature(item) {
    let redFR = scale(item.BrakeTemperatureFrontRight, 0, 50, 50, 255);
    let greenFR = scale(item.BrakeTemperatureFrontRight, 0, 50, 255, 50);

    let redFL = scale(item.BrakeTemperatureFrontLeft, 0, 50, 50, 255);
    let greenFL = scale(item.BrakeTemperatureFrontLeft, 0, 50, 255, 50);

    let redBR = scale(item.BrakeTemperatureRearRight, 0, 50, 50, 255);
    let greenBR = scale(item.BrakeTemperatureRearRight, 0, 50, 255, 50);

    let redBL = scale(item.BrakeTemperatureRearLeft, 0, 50, 50, 255);
    let greenBL = scale(item.BrakeTemperatureRearLeft, 0, 50, 255, 50);

    $("#wheel-FR").css("fill", "rgb(" + redFR + ", " + greenFR + ", 0)");
    $("#wheel-FL").css("fill", "rgb(" + redFL + ", " + greenFL + ", 0)");
    $("#wheel-BR").css("fill", "rgb(" + redBR + ", " + greenBR + ", 0)");
    $("#wheel-BL").css("fill", "rgb(" + redBL + ", " + greenBL + ", 0)");
}

setInterval(function () {
    $.getJSON("data/data.json", function (json) {
        drawGoogleChart(json);
        updateLap(json);
        updateGear(json);
        drawForcesChart(json);
        updateSteering(json);
        updateWheelTemperature(json);
    });
}, refreshRate);

function updateLap(item) {
    $("#lap").text(item.Lap);
}
function updateGear(item) {
    if (item.Gear == 0) {
        $("#gear").addClass("red-text");
        $("#gear").text("R");
    } else if (item.Gear == 1) {
        $("#gear").addClass("red-text");
        $("#gear").text("N");
    } else {
        $("#gear").removeClass("red-text");
        $("#gear").text(item.Gear - 1);
    }
}

function updateSteering(item) {
    $("#steering").val(item.Steer);
}

function drawGoogleChart(item) {
    google.charts.load('current', { 'packages': ['gauge'] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        //SPEED CHART
        var chartData = [
            ['Label', 'Value'],
            ['Speed', item.Speed]
        ];
        var data = google.visualization.arrayToDataTable(chartData);
        var options = {
            width: 200, height: 200,
            redFrom: 375, redTo: 500,
            yellowFrom: 250, yellowTo: 375,
            minorTicks: 5,
            min: 0,
            max: 500
        };
        var chart = new google.visualization.Gauge(document.getElementById('speed-chart'));
        chart.draw(data, options);

        //RPM CHART
        chartData = [
            ['Label', 'Value'],
            ['RPM', item.EngineRevs]
        ];
        data = google.visualization.arrayToDataTable(chartData);
        options = {
            width: 200, height: 200,
            redFrom: 11250, redTo: 15000,
            yellowFrom: 7500, yellowTo: 11250,
            minorTicks: 5,
            min: 0,
            max: 15000
        };
        chart = new google.visualization.Gauge(document.getElementById('rpm-chart'));
        chart.draw(data, options);

        //BRAKE & THROTTLE CHART
        google.charts.load("current", { packages: ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {

            var preData = [
                ["Data", "Value", { role: "style" }],
                ["Brake", item.Brake, "red"],
                ["Throttle", item.Throttle, "green"]
            ];

            data = google.visualization.arrayToDataTable(preData);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2]);

            options = {
                width: 200,
                height: 200,
                bar: { groupWidth: "95%" },
                legend: { position: "none" },
            };
            chart = new google.visualization.ColumnChart(document.getElementById("brake-throttle-chart"));
            chart.draw(view, options);
        }
    }
}

function drawForcesChart(item) {

    var stage = document.getElementById('forces-chart'); // Get the canvas element by Id
    var ctx = stage.getContext('2d'); // Canvas 2d rendering context

    ctx.clearRect(0, 0, 500, 500);

    var x = 90;
    var y = 90;
    var wid = 20;
    var hei = 20;

    x = scale(item.LateralAcceleration, -4, 4, 1, 180);
    y = scale(item.LongitudinalAcceleration, -4, 4, 1, 180);

    //Draw Rectangle function		
    function drawRect(x, y, wid, hei) {
        ctx.fillStyle = '#666'; // Fill color of rectangle drawn
        ctx.fillRect(x, y, wid, hei); //This will draw a rectangle of 20x20
    }

    drawRect(x, y, wid, hei); //Drawing rectangle on initial load

    //move rectangle inside the canvas using arrow keys
    window.onkeydown = function (event) {
        var keyPr = event.keyCode; //Key code of key pressed

        if (keyPr === 39 && x <= 180) {
            x = x + 1; //right arrow
        }
        else if (keyPr === 37 && x > 1) {
            x = x - 1; //left arrow
        }
        else if (keyPr === 38 && y > 1) {
            y = y - 1; //top arrow
        }
        else if (keyPr === 40 && y <= 180) {
            y = y + 1; //bottom arrow
        }
        drawRect(x, y, wid, hei);
    };
}

const scale = (num, in_min, in_max, out_min, out_max) => {
    return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}