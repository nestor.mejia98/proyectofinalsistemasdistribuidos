var amqp = require('amqplib/callback_api');
var fs = require('fs');

amqp.connect('amqp://user:password@172.27.14.241', function (err, conn) {

  conn.createChannel(function (err, ch) {
    var q = 'YourQueue';
    
    //Change durable to false if using local RabbitMQ
    ch.assertQueue(q, { durable: true });
    ch.consume(q, function (msg) {
      var message = msg.content.toString();
      fs.writeFile('../JavaScriptDashboard/app/data/data.json', message, function (err) {
        if (err) throw err;
      });

    }, { noAck: true });
  });
});