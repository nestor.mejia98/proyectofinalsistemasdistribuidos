﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using System.Reflection;

namespace Entities

{
    [Serializable]
    public struct TelemetryPacket : ISerializable
    {

        public float Time { get; set; }
        public float LapTime { get; set; }
        public float LapDistance;
        public float Distance;
        [XmlIgnore]
        public float X;
        [XmlIgnore]
        public float Y;
        [XmlIgnore]
        public float Z;
        public float Speed { get; set; }
        [XmlIgnore]
        public float WorldSpeedX;
        [XmlIgnore]
        public float WorldSpeedY;
        [XmlIgnore]
        public float WorldSpeedZ;
        [XmlIgnore]
        public float XR;
        [XmlIgnore]
        public float Roll;
        [XmlIgnore]
        public float ZR;
        [XmlIgnore]
        public float XD;
        [XmlIgnore]
        public float Pitch;
        [XmlIgnore]
        public float ZD;
        [XmlIgnore]
        public float SuspensionPositionRearLeft;
        [XmlIgnore]
        public float SuspensionPositionRearRight;
        [XmlIgnore]
        public float SuspensionPositionFrontLeft;
        [XmlIgnore]
        public float SuspensionPositionFrontRight;
        [XmlIgnore]
        public float SuspensionVelocityRearLeft;
        [XmlIgnore]
        public float SuspensionVelocityRearRight;
        [XmlIgnore]
        public float SuspensionVelocityFrontLeft;
        [XmlIgnore]
        public float SuspensionVelocityFrontRight;
        [XmlIgnore]
        public float WheelSpeedBackLeft;
        [XmlIgnore]
        public float WheelSpeedBackRight;
        [XmlIgnore]
        public float WheelSpeedFrontLeft;
        [XmlIgnore]
        public float WheelSpeedFrontRight;
        [XmlIgnore]
        public float Throttle { get; set; }
        [XmlIgnore]
        public float Steer;
        [XmlIgnore]
        public float Brake;
        [XmlIgnore]
        public float Clutch;
        [XmlIgnore]
        public float Gear;
        [XmlIgnore]
        public float LateralAcceleration;
        [XmlIgnore]
        public float LongitudinalAcceleration;
        public float Lap;
        [XmlIgnore]
        public float EngineRevs;

        /* New Fields in Patch 12 */
        [XmlIgnore]
        public float NewField1;     // Always 1?
        [XmlIgnore]
        public float RacePosition;     // Position in race
        [XmlIgnore]
        public float KersRemaining;     // Kers Remaining
        [XmlIgnore]
        public float KersRecharge;     // Always 400000? 
        [XmlIgnore]
        public float DrsStatus;     // Drs Status
        [XmlIgnore]
        public float Difficulty;     // 2 = Medium or Easy, 1 = Hard, 0 = Expert
        [XmlIgnore]
        public float Assists;     // 0 = All assists are off.  1 = some assist is on.
        [XmlIgnore]
        public float FuelRemaining;      // Not sure if laps or Litres?
        [XmlIgnore]
        public float SessionType;   // 9.5 = race, 10 = time trail / time attack, 170 = quali, practice, championsmode
        [XmlIgnore]
        public float NewField10;
        [XmlIgnore]
        public float Sector;    // Sector (0, 1, 2)
        [XmlIgnore]
        public float TimeSector1;    // Time Intermediate 1
        [XmlIgnore]
        public float TimeSector2;    // Time Intermediate 2
        [XmlIgnore]
        public float BrakeTemperatureRearLeft;
        [XmlIgnore]
        public float BrakeTemperatureRearRight;
        [XmlIgnore]
        public float BrakeTemperatureFrontLeft;
        [XmlIgnore]
        public float BrakeTemperatureFrontRight;
        [XmlIgnore]
        public float NewField18;    // Always 0?
        [XmlIgnore]
        public float NewField19;    // Always 0?
        [XmlIgnore]
        public float NewField20;    // Always 0?
        [XmlIgnore]
        public float NewField21;    // Always 0?
        [XmlIgnore]
        public float CompletedLapsInRace;    // Number of laps Completed (in GP only)
        [XmlIgnore]
        public float TotalLapsInRace;    // Number of laps in GP (GP only)
        [XmlIgnore]
        public float TrackLength;    // Track Length
        [XmlIgnore]
        public float PreviousLapTime;    // Lap time of previous lap

        //  The next three fields are new for F1 2013

        [XmlIgnore]
        public float NewField26;    // Always 0?
        [XmlIgnore]
        public float NewField27;    // Always 0?
        [XmlIgnore]
        public float NewField28;    // Always 0?

        /* End new Fields */

        [XmlIgnore]
        public float SpeedInKmPerHour
        {
            get { return Speed * 3.60f; }
        }

        [XmlIgnore]
        public bool IsSittingInPits
        {
            get { return Math.Abs(LapTime - 0) < 0.00001f && Math.Abs(Speed - 0) < 0.00001f; }
        }

        [XmlIgnore]
        public bool IsInPitLane
        {
            get { return Math.Abs(LapTime - 0) < 0.00001f; }
        }

        [XmlIgnore]
        public string SessionTypeName
        {
            get
            {
                if (Math.Abs(this.SessionType - 9.5f) < 0.0001f)
                    return "Race";
                if (Math.Abs(this.SessionType - 10f) < 0.0001f)
                    return "Time Trial";
                if (Math.Abs(this.SessionType - 170f) < 0.0001f)
                    return "Qualifying or Practice";
                return "Other";
            }
        }

        public TelemetryPacket(SerializationInfo info, StreamingContext context)
        {
            Time = (float)info.GetValue("Time", 0.00001f.GetType());
            LapTime = (float)info.GetValue("LapTime", 0.00001f.GetType());
            LapDistance = (float)info.GetValue("LapDistance", 0.00001f.GetType());
            Distance = (float)info.GetValue("Distance", 0.00001f.GetType());
            Speed = (float)info.GetValue("Speed", 0.00001f.GetType());
            Lap = (float)info.GetValue("Lap", 0.00001f.GetType());
            X = (float)info.GetValue("X", 0.00001f.GetType());
            Y = (float)info.GetValue("Y", 0.00001f.GetType());
            Z = (float)info.GetValue("Z", 0.00001f.GetType());
            WorldSpeedX = (float)info.GetValue("WorldSpeedX", 0.00001f.GetType());
            WorldSpeedY = (float)info.GetValue("WorldSpeedY", 0.00001f.GetType());
            WorldSpeedZ = (float)info.GetValue("WorldSpeedZ", 0.00001f.GetType());
            XR = (float)info.GetValue("XR", 0.00001f.GetType());
            Roll = (float)info.GetValue("Roll", 0.00001f.GetType());
            ZR = (float)info.GetValue("ZR", 0.00001f.GetType());
            XD = (float)info.GetValue("XD", 0.00001f.GetType());
            Pitch = (float)info.GetValue("Pitch", 0.00001f.GetType());
            ZD = (float)info.GetValue("ZD", 0.00001f.GetType());
            SuspensionPositionRearLeft = (float)info.GetValue("SuspensionPositionRearLeft", 0.00001f.GetType());
            SuspensionPositionRearRight = (float)info.GetValue("SuspensionPositionRearRight", 0.00001f.GetType());
            SuspensionPositionFrontLeft = (float)info.GetValue("SuspensionPositionFrontLeft", 0.00001f.GetType());
            SuspensionPositionFrontRight = (float)info.GetValue("SuspensionPositionFrontRight", 0.00001f.GetType());
            SuspensionVelocityRearLeft = (float)info.GetValue("SuspensionVelocityRearLeft", 0.00001f.GetType());
            SuspensionVelocityRearRight = (float)info.GetValue("SuspensionVelocityRearRight", 0.00001f.GetType());
            SuspensionVelocityFrontLeft = (float)info.GetValue("SuspensionVelocityFrontLeft", 0.00001f.GetType());
            SuspensionVelocityFrontRight = (float)info.GetValue("SuspensionVelocityFrontRight", 0.00001f.GetType());
            WheelSpeedBackLeft = (float)info.GetValue("WheelSpeedBackLeft", 0.00001f.GetType());
            WheelSpeedBackRight = (float)info.GetValue("WheelSpeedBackRight", 0.00001f.GetType());
            WheelSpeedFrontLeft = (float)info.GetValue("WheelSpeedFrontLeft", 0.00001f.GetType());
            WheelSpeedFrontRight = (float)info.GetValue("WheelSpeedFrontRight", 0.00001f.GetType());
            Throttle = (float)info.GetValue("Throttle", 0.00001f.GetType());
            Steer = (float)info.GetValue("Steer", 0.00001f.GetType());
            Brake = (float)info.GetValue("Brake", 0.00001f.GetType());
            Clutch = (float)info.GetValue("Clutch", 0.00001f.GetType());
            Gear = (float)info.GetValue("Gear", 0.00001f.GetType());
            LateralAcceleration = (float)info.GetValue("LateralAcceleration", 0.00001f.GetType());
            LongitudinalAcceleration = (float)info.GetValue("LongitudinalAcceleration", 0.00001f.GetType());
            EngineRevs = (float)info.GetValue("EngineRevs", 0.00001f.GetType());

            NewField1 = (float)info.GetValue("NewField1", 0.00001f.GetType());
            RacePosition = (float)info.GetValue("RacePosition", 0.00001f.GetType());
            KersRemaining = (float)info.GetValue("KersRemaining", 0.00001f.GetType());
            KersRecharge = (float)info.GetValue("KersRecharge", 0.00001f.GetType());
            DrsStatus = (float)info.GetValue("DrsStatus", 0.00001f.GetType());
            Difficulty = (float)info.GetValue("Difficulty", 0.00001f.GetType());
            Assists = (float)info.GetValue("Assists", 0.00001f.GetType());
            FuelRemaining = (float)info.GetValue("FuelRemaining", 0.00001f.GetType());
            SessionType = (float)info.GetValue("SessionType", 0.00001f.GetType());
            NewField10 = (float)info.GetValue("NewField10", 0.00001f.GetType());
            Sector = (float)info.GetValue("Sector", 0.00001f.GetType());
            TimeSector1 = (float)info.GetValue("TimeSector1", 0.00001f.GetType());
            TimeSector2 = (float)info.GetValue("TimeSector2", 0.00001f.GetType());
            BrakeTemperatureRearLeft = (float)info.GetValue("BrakeTemperatureRearLeft", 0.00001f.GetType());
            BrakeTemperatureRearRight = (float)info.GetValue("BrakeTemperatureRearRight", 0.00001f.GetType());
            BrakeTemperatureFrontLeft = (float)info.GetValue("BrakeTemperatureFrontLeft", 0.00001f.GetType());
            BrakeTemperatureFrontRight = (float)info.GetValue("BrakeTemperatureFrontRight", 0.00001f.GetType());
            NewField18 = (float)info.GetValue("NewField18", 0.00001f.GetType());
            NewField19 = (float)info.GetValue("NewField19", 0.00001f.GetType());
            NewField20 = (float)info.GetValue("NewField20", 0.00001f.GetType());
            NewField21 = (float)info.GetValue("NewField21", 0.00001f.GetType());
            CompletedLapsInRace = (float)info.GetValue("CompletedLapsInRace", 0.00001f.GetType());
            TotalLapsInRace = (float)info.GetValue("TotalLapsInRace", 0.00001f.GetType());
            TrackLength = (float)info.GetValue("TrackLength", 0.00001f.GetType());
            PreviousLapTime = (float)info.GetValue("PreviousLapTime", 0.00001f.GetType());
            NewField26 = (float)info.GetValue("NewField26", 0.00001f.GetType());
            NewField27 = (float)info.GetValue("NewField27", 0.00001f.GetType());
            NewField28 = (float)info.GetValue("NewField28", 0.00001f.GetType());
        }



        public override string ToString()
        {
            var sb = new StringBuilder();

            var fields = this.GetType().GetFields();
            foreach (var field in fields)
            {
                sb.AppendFormat("{0}({1}) : ", field.Name, field.GetValue(this));
            }

            var props = this.GetType().GetProperties();
            foreach (var prop in props)
                sb.AppendFormat("{0}({1}) : ", prop.Name, prop.GetValue(this, null));

            return sb.ToString();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Time", Time);
            info.AddValue("LapTime", LapTime);
            info.AddValue("LapDistance", LapDistance);
            info.AddValue("Distance", Distance);
            info.AddValue("X", X);
            info.AddValue("Y", Y);
            info.AddValue("Z", Z);
            info.AddValue("Speed", Speed);
            info.AddValue("WorldSpeedX", WorldSpeedX);
            info.AddValue("WorldSpeedY", WorldSpeedY);
            info.AddValue("WorldSpeedZ", WorldSpeedZ);
            info.AddValue("XR", XR);
            info.AddValue("Roll", Roll);
            info.AddValue("ZR", ZR);
            info.AddValue("XD", XD);
            info.AddValue("Pitch", Pitch);
            info.AddValue("ZD", ZD);
            info.AddValue("SuspensionPositionRearLeft", SuspensionPositionRearLeft);
            info.AddValue("SuspensionPositionRearRight", SuspensionPositionRearRight);
            info.AddValue("SuspensionPositionFrontLeft", SuspensionPositionFrontLeft);
            info.AddValue("SuspensionPositionFrontRight", SuspensionPositionFrontRight);
            info.AddValue("SuspensionVelocityRearLeft", SuspensionVelocityRearLeft);
            info.AddValue("SuspensionVelocityRearRight", SuspensionVelocityRearRight);
            info.AddValue("SuspensionVelocityFrontLeft", SuspensionVelocityFrontLeft);
            info.AddValue("SuspensionVelocityFrontRight", SuspensionVelocityFrontRight);
            info.AddValue("WheelSpeedBackLeft", WheelSpeedBackLeft);
            info.AddValue("WheelSpeedBackRight", WheelSpeedBackRight);
            info.AddValue("WheelSpeedFrontLeft", WheelSpeedFrontLeft);
            info.AddValue("WheelSpeedFrontRight", WheelSpeedFrontRight);
            info.AddValue("Throttle", Throttle);
            info.AddValue("Steer", Steer);
            info.AddValue("Brake", Brake);
            info.AddValue("Clutch", Clutch);
            info.AddValue("Gear", Gear);
            info.AddValue("LateralAcceleration", LateralAcceleration);
            info.AddValue("LongitudinalAcceleration", LongitudinalAcceleration);
            info.AddValue("Lap", Lap);
            info.AddValue("EngineRevs", EngineRevs);

            info.AddValue("NewField1", NewField1);
            info.AddValue("RacePosition", RacePosition);
            info.AddValue("KersRemaining", KersRemaining);
            info.AddValue("KersRecharge", KersRecharge);
            info.AddValue("DrsStatus", DrsStatus);
            info.AddValue("Difficulty", Difficulty);
            info.AddValue("Assists", Assists);
            info.AddValue("FuelRemaining", FuelRemaining);
            info.AddValue("SessionType", SessionType);
            info.AddValue("NewField10", NewField10);
            info.AddValue("Sector", Sector);
            info.AddValue("TimeSector1", TimeSector1);
            info.AddValue("TimeSector2", TimeSector2);
            info.AddValue("BrakeTemperatureRearLeft", BrakeTemperatureRearLeft);
            info.AddValue("BrakeTemperatureRearRight", BrakeTemperatureRearRight);
            info.AddValue("BrakeTemperatureFrontLeft", BrakeTemperatureFrontLeft);
            info.AddValue("BrakeTemperatureFrontRight", BrakeTemperatureFrontRight);
            info.AddValue("NewField18", NewField18);
            info.AddValue("NewField19", NewField19);
            info.AddValue("NewField20", NewField20);
            info.AddValue("NewField21", NewField21);
            info.AddValue("CompletedLapsInRace", CompletedLapsInRace);
            info.AddValue("TotalLapsInRace", TotalLapsInRace);
            info.AddValue("TrackLength", TrackLength);
            info.AddValue("PreviousLapTime", PreviousLapTime);
            info.AddValue("NewField26", NewField26);
            info.AddValue("NewField27", NewField27);
            info.AddValue("NewField28", NewField28);
        }
    }

    public static class Extensions
    {
        public static bool EntryExists(this SerializationInfo info, string fieldName)
        {
            SerializationInfoEnumerator e = info.GetEnumerator();
            while (e.MoveNext())
            {
                if (e.Name.ToUpper() == fieldName.ToUpper())
                    return true;
            }
            return false;
        }
    }

}