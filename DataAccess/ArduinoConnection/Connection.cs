﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using Entities;

namespace ArduinoConnection
{
    public class Connection
    {
        bool isConnected = false;
        SerialPort port;

        public Connection() {
            this.connectToArduino();
        }

        private void connectToArduino()
        {
            isConnected = true;
            string selectedPort = "COM7"; //esto debemos cambiarlo de acuerdo al puerto que le asigne la maquina al arduino
            port = new SerialPort(selectedPort, 9600, Parity.None, 8, StopBits.One);
            port.Open();
            port.Write("#STAR\n");
        }

        private void disconnectFromArduino()
        {
            isConnected = false;
            port.Write("#STOP\n");
            port.Close();
        }

        public async void ReceivePacket(TelemetryPacket packet) {
            Task tarea = new Task(() => setServosPosition(packet.X, packet.Y, packet.Z, packet.XR, 0, packet.ZR));
            tarea.Start();
            await tarea;
            
        }

        public void setServosPosition(float x, float y, float z, float rx, float ry, float rz)
        {
            if (isConnected)
            {
                //port.Write("#SERX" + x.ToString("N2") + "\n");
                //port.Write("#SERY" + y.ToString("N2") + "\n");
                //port.Write("#SERZ" + z.ToString("N2") + "\n");
                //port.Write("#SER1" + rx.ToString("N2") + "\n");
                //port.Write("#SER2" + ry.ToString("N2") + "\n");
                //port.Write("#SER3" + rz.ToString("N2") + "\n");
                //port.Write("#SERF\n"); //se termino el envio de  la data

                port.Write("SETPOSITIONS" + " " + x.ToString("0.0000") + " "
                    + " " + y.ToString("0.0000") + " "
                    + " " + z.ToString("0.0000") + " "
                    + " " + rx.ToString("0.0000") + " "
                    + " " + ry.ToString("0.0000") + " "
                    + " " + rz.ToString("0.0000") + " " +
                    "\n");

  
            }
            
        }

    }
}
