﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using ServiceBrokerPublisher;
using Entities;
using ArduinoConnection;

namespace UdpReceiver
{
    class Server
    {

        public delegate void DataReceived(object sender, ReceivedDataArgs args);

        public event DataReceived DataReceivedEvent;

        public delegate void SendPacket(TelemetryPacket packet);
        public event SendPacket SendPacketEvent;

        public void Listen()
        {

            UdpClient listener = new UdpClient(20777);

            IPEndPoint serverEP = new IPEndPoint(IPAddress.Any, 20777);

            //Inicio el service broker
            Publisher publisher = new Publisher();

            this.SendPacketEvent += publisher.PublishPacketToServiceBroker;

            //Conectar al Arduino
           Connection connection = new Connection();
            this.SendPacketEvent += connection.ReceivePacket;
            int i = 1;

            while (true)
            {

                byte[] data = listener.Receive(ref serverEP);
                var latestData = PacketUtilities.ConvertToPacket(data);


                Console.WriteLine(i+ " - Lap time:" + latestData.LapTime + "\n" +
                    "Speed: " + latestData.SpeedInKmPerHour + "\n" +
                    "Throttle: " + (latestData.Throttle * 100) + "%\n" +
                    "Brake: " + (latestData.Brake * 100) + "%\n" +
                    "Gear: " + latestData.Gear + "\n" +
                    "Long: " + latestData.LongitudinalAcceleration + "\n" +
                    "Lat: " + latestData.LateralAcceleration+"\n");

                //SendPacketEvent(latestData);

                foreach (SendPacket packetHandler in SendPacketEvent.GetInvocationList()) {
                    packetHandler.BeginInvoke(latestData, null, null);
                }

                

                //Parallel.ForEach(SendPacketEvent.GetInvocationList(), SendPacket.CreateDelegate(sENDpAC) => {

                //});
                i++;
                //RaiseDataReceived(new ReceivedDataArgs(serverEP.Address, serverEP.Port, data));

            }

        }

        private void RaiseDataReceived(ReceivedDataArgs args)
        {

            DataReceivedEvent?.Invoke(this, args);

        }

    }

    public static class PacketUtilities
    {

        public static TelemetryPacket ConvertToPacket(byte[] bytes)
        {

            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            var stuff = (TelemetryPacket)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(TelemetryPacket));
            handle.Free();
            return stuff;

        }

    }

}
