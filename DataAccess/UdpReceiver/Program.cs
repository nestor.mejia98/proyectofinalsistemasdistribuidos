﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace UdpReceiver
{
    class Program
    {


        static void Main(string[] args)
        {

            Console.WriteLine("Starting receiver");
            Server server = new Server();
            ProccessDataForInterface pdfi = new ProccessDataForInterface();

            //Iniciar el hilo del servidor
            Thread serverThread = new Thread(() => server.Listen());
            serverThread.Start();


            //Iniciar el hilo de los data handler
            Thread proccessDataForInterfaceThread = new Thread(() => pdfi.SusbscribeToEvent(server));
            proccessDataForInterfaceThread.Start();

            



            while (true)
            {
                Thread.Sleep(100);
            }



        }



    }
}