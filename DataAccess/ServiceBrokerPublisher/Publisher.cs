﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using System.Web.Script.Serialization;
using Entities;

namespace ServiceBrokerPublisher
{
    public class Publisher
    {
        private CommonService _commonService;
        private IConnection _connection;
        private static IModel _model;

        public Publisher() {

            _commonService = new CommonService();
            _connection = _commonService.GetRabbitMqConnection();
            _model = _connection.CreateModel();
            _model.QueueDeclare(CommonService.SerialisationQueueName, true, false, false, null);

        }

        public async void PublishPacketToServiceBroker(TelemetryPacket telemetryPacket)
        {
            Task tarea = new Task(() => SendPacket(telemetryPacket));
            tarea.Start();
            await tarea;

            
            
        }

        private void SendPacket(TelemetryPacket telemetryPacket) {
            IBasicProperties basicProperties = _model.CreateBasicProperties();
            basicProperties.SetPersistent(true);
            String jsonified = new JavaScriptSerializer().Serialize(telemetryPacket);
            byte[] customerBuffer = Encoding.UTF8.GetBytes(jsonified);
            _model.BasicPublish("", CommonService.SerialisationQueueName, basicProperties, customerBuffer);
        }

    }
}
