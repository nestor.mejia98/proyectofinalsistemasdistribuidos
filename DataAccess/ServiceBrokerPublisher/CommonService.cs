﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;


namespace ServiceBrokerPublisher
{
    class CommonService
    {

        private string _hostName = "localhost";
        private string _userName = "broker";
        private string _password = "sistemas";

        public static string SerialisationQueueName = "TelemetryInfoQueue";

        public IConnection GetRabbitMqConnection()
        {
            ConnectionFactory connectionFactory = new ConnectionFactory
            {
                HostName = _hostName,
                UserName = _userName,
                Password = _password
            };

            return connectionFactory.CreateConnection();
        }

    }
}
