/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, Image } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
var { height } = Dimensions.get('window');
 
var box_count = 3;
var box_height = height / box_count;
export default class App extends Component<Props> {

  constructor(props) {
    super(props);

    this.state = {
      telemetry: {
        Speed: 0,
        Gear: 0,
        OilTemp: 0,
        LapTime: 0
      }
    }

  }

  componentDidMount() {
    this.initializeRefreshing();
  }

  initializeRefreshing() {
    setInterval(this.fetchJSONfromNode.bind(this), 100);
  }

  fetchJSONfromNode() {
    let self = this;
    fetch('http://172.27.62.228/Sistemas%20Distribuidos%20Proyecto/JS/app/data/data.json')
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        self.setState({ telemetry: responseJson })
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    console.log(this.state.telemetry)
    return (

      <View style={styles.container}>
        <View style={[styles.box, styles.box1]}>
        <Image
          source={require('./components/img/crono.png')}
          style = {styles.imageStyle}
        />
        <Text style={styles.welcome}>{this.state.telemetry.LapTime} s</Text>
        </View>
        <View style={[styles.box, styles.box2]}>
        <Image
          source={require('./components/img/gear.png')}
          style = {styles.imageStyle}
        />
        <Text style={styles.welcome}>{this.state.telemetry.Gear}</Text>
        </View>
        <View style={[styles.box, styles.box3]}>
        <Image
          source={require('./components/img/speedo.png')}
          style = {styles.imageStyle}
        />
        <Text style={styles.welcome}>{this.state.telemetry.Speed}</Text>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerd: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 50,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  box: {
    height: box_height,
    flexDirection : 'row'
  },
  box1: {
    backgroundColor: '#2196F3',
    
  },
  box2: {
    backgroundColor: '#8BC34A'
  },
  box3: {
    backgroundColor: '#e3aa1a'
  },

  imageStyle : {
    height : '100%',
    width:'50%'
  }

});
